import { BudgetController } from './budget-controller'

export function CurrentBudget () {
  let budget

  function loadBudget (budgetId, db) {
    unloadBudget()

    let budgetManager = db.budget(budgetId)

    return Promise.all([
      budgetManager.budget(),
      budgetManager.categories.all(),
      budgetManager.masterCategories.all(),
      budgetManager.payees.all(),
      db.budgets.get(budgetId)
    ]).then(([monthManager, categories, masterCategories, payees, budget]) => {
      monthManager.propagateRolling(Object.keys(categories))
      return new BudgetController(budgetId, db, budget, monthManager, categories, masterCategories, payees)
    }).catch(e => {
      throw e
    })
  }

  function unloadBudget () {
    if (budget) {
      budget.unsubscribe()
      budget = null
    }
  }

  return {
    budget,
    loadBudget,
    unloadBudget
  }
}
