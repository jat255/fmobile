import { BehaviorSubject } from 'rxjs/BehaviorSubject'

export function Theme () {
  const $themeChanged = new BehaviorSubject(getTheme())

  function getTheme () {
    return localStorage.getItem('theme')
  }

  function setTheme (theme) {
    document.getElementById('theme-icon').setAttribute('href', 'https://fmobile.asromzek.com/favicon-' + theme + '.ico')
    document.getElementById('theme-color').setAttribute('href', 'https://www.w3schools.com/lib/w3-theme-' + theme + '.css')
    $themeChanged.next(theme)
    localStorage.setItem('theme', theme)
  }

  return {
    getTheme,
    setTheme,
    $themeChanged
  }
}
